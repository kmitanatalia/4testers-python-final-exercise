class Order:
    def __init__(self, customer_email):
        self.customer_email = customer_email
        self.products = []
        self.purchased = False

    def add_product(self, product):
        self.products.append(product)

    def get_total_price(self):
        total_price_products = sum(product.get_price() for product in self.products)
        return total_price_products

    def get_total_quantity_of_products(self):
        total_quantity_products = sum(product.quantity for product in self.products)
        return total_quantity_products

    def purchase(self):
        self.purchased = True


class Product:
    def __init__(self, name, unit_price, quantity=1):
        self.name = name
        self.unit_price = unit_price
        self.quantity = quantity

    def get_price(self):
        return self.unit_price * self.quantity


if __name__ == '__main__':
    test_product = Product("Shoes", 30, 3)
    print("Product price:", test_product.get_price())

    test_order = Order('adrian@example.com')
    print("Customer email adress:", test_order.customer_email)

    test_order.add_product(test_product)
    print("To pay:", test_order.get_total_price())

    test_order.purchase()
    print("Order Purchased:", test_order.purchased)

    add_new_product1 = Product("T-shirt", 50, 3)
    add_new_product2 = Product("Bag", 10, 2)
    test_order.add_product(add_new_product1)
    test_order.add_product(add_new_product2)
    print("Total price:", test_order.get_total_price(), "$")
    print("Amount of products:", test_order.get_total_quantity_of_products())
